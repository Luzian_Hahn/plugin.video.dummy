import sys
import logging
import json
import xbmcgui
import xbmcplugin

LOGFILE="/tmp/plugin.video.dummy.log"
LOG = logging.getLogger(__name__)
LOG.addHandler(logging.FileHandler(LOGFILE))
LOG.addHandler(logging.StreamHandler())


__url__ = sys.argv[0]
# Get the plugin handle as an integer number.
__handle__ = int(sys.argv[1])


def list_dummy_menu():
    elements = list("abcdefgh")
    list_items = map(lambda e: xbmcgui.ListItem(label=e), elements)
    urls = map(lambda e: "{0}?something={1}".format(__url__, e), elements)
    is_folder_flags = map(lambda e: True, elements)

    listing = list(zip(urls, list_items, is_folder_flags))

    xbmcplugin.addDirectoryItems(__handle__, listing, len(listing))

    xbmcplugin.endOfDirectory(__handle__)


def main(paramstring):
    list_dummy_menu()


if __name__ == "__main__":
    LOG.warning(sys.argv)
    LOG.warning(sys.version_info)
    main(sys.argv[2])
